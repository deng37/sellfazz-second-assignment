import React from 'react';
import { FlatList, StyleSheet, Text, View, Button } from 'react-native';
import axios from 'axios'

export default class Home extends React.Component {
  async componentDidMount() {
    const response = await axios.get("http://192.168.1.100:8080/products",
      {
       headers: {
         'Authorization' : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InEiLCJpZCI6IjIiLCJpYXQiOjE1NTU5Mjg3ODV9.vIdEYZr7FzJ6t2AbiPHUFsEn7mqj1rfdGXIO66IVZCE',
         'Content-Type' : 'application/json',
         'Cache-Control' : 'private, no-cache, no-store, must-revalidate'
       } 
      }
    )
    const result = await response.data
    if (response) {
      this.setState({ productList: result })
    }
  }

  public addProduct() {

  }

  render() {
    return (
      <View>
        <Button
          title="Add Product"
          onPress={() =>
            this.addProduct()
          }
        />
        <View>
          {
            this.state.productList.map( (product) => {
              return (
                <Text>{ product.name }</Text>
              )
            })
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
