import React from 'react';
import { TextInput, StyleSheet, Text, View, Button } from 'react-native';
import thunk from 'redux-thunk'
import axios from 'axios'

export default class Home extends React.Component {
  public setRegisterUser() {
    console.log("AA1")
    axios.post("http://192.168.1.100:8080/register",
      {
        email: this.state.userEmail,
        password: this.state.userPassword
      }
    ).then(response => {
      console.log({ serverports: response.data })
    })
    .catch(function (error) {
      console.log(error)
    })

    axios.post("http://192.168.1.100:8080/login",
      {
        email: this.state.userEmail,
        password: this.state.userPassword
      },
      {
        headers: {
          'Authorization': 'Bearer AkuSihMasihCinta',
          'Content-Type': 'application/json'
        }
      }
    ).then(response => {
      console.log({ serverports: response.data })
      if (response.data) {
        this.processRegisteredUser(response.data)
      }
    })
    .catch(function (error) {
      console.log(error)
    })
  }

  public processRegisteredUser(response) {
    this.setState({userToken: response.token})
    this.props.navigation.navigate('Main')
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, borderRadius: 5, paddingVertical: 5, paddingHorizontal: 5, textAlign: 'center', marginBottom: 5}}
          placeholder="email"
          onChangeText={(userEmail) => this.setState({userEmail})}
          // value="{this.state.text}"
        />
        <TextInput
          style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, borderRadius: 5, paddingVertical: 5, paddingHorizontal: 5, textAlign: 'center', marginBottom: 5}}
          placeholder="password"
          onChangeText={(userPassword) => this.setState({userPassword})}
          // value="{this.state.text}"
        />
        <Button
          title="Register"
          onPress={() =>
            this.setRegisterUser()
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
