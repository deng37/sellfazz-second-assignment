import React from 'react';
import { TextInput, StyleSheet, Text, View, Button } from 'react-native';
import axios from 'axios'
import { mainReducer } from '../src/reducers'
import { mainAction } from '../src/actions'
import { createStore } from 'redux'

export default class Home extends React.Component {
  storeAction = new mainAction()
  storeReducer = new mainReducer()
  store = createStore(this.storeReducer.todoApp)
  firstLoop = true

  public handleLogin() {
    this.store.dispatch(this.storeAction.setUser('USER', {    // insert new user
      type: "LOGIN",
      payload: {
        email: this.state.userEmail,
        password: this.state.userPassword
      }
    }))
    this.props.navigation.navigate('Main')
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, borderRadius: 5, paddingVertical: 5, paddingHorizontal: 5, textAlign: 'center', marginBottom: 5}}
          placeholder="email"
          onChangeText={(userEmail) => this.setState({userEmail})}
          // value="{this.state.text}"
        />
        <TextInput
          style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, borderRadius: 5, paddingVertical: 5, paddingHorizontal: 5, textAlign: 'center', marginBottom: 5}}
          placeholder="password"
          onChangeText={(userPassword) => this.setState({userPassword})}
          // value="{this.state.text}"
        />
        <Button
          title="Login"
          onPress={() =>
            this.handleLogin()
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
