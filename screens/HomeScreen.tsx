import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default class Home extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>at Home, you can do</Text>
        <Button
          title="Login"
          onPress={() =>
            this.props.navigation.navigate('Login')
          }
        />
        <Button
          title="Register"
          onPress={() =>
            this.props.navigation.navigate('Register')
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
