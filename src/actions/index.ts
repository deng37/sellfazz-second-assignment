/*
 * action creators
 */
export class mainAction {
	public setUser(prefixName: string, data: {type: string, payload: { email: string, password: string } | null}) {		// action to manage list user
	  return { type: prefixName + '.' + data.type, payload: data.payload }
	}
}