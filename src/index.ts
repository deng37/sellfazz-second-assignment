import { createStore } from 'redux'
import { mainReducer } from './reducers'
import { mainAction } from './actions'
import { USERLIST, VisibilityFilters } from './constants'

// Calling Action and Reducer Class
let storeAction = new mainAction();
let storeReducer = new mainReducer();

// Creating Store
const store = createStore(storeReducer.todoApp)

// Every time the state changes, log it
// Note that subscribe() returns a function for unregistering the listener
const unsubscribe = store.subscribe(() => console.log("\n" + "%j", store.getState()))

// Dispatch some actions
store.dispatch(storeAction.setVisibilityFilter(VisibilityFilters.SHOW_COMPLETED))
store.dispatch(storeAction.setListManager(USERLIST, {		// insert new user
	type: "UPSERT",
	payload: {
		id: 3,
		name: "Doramomon"
	}
}))
store.dispatch(storeAction.setListManager(USERLIST, {		// update existing user
	type: "UPSERT",
	payload: {
		id: 3,
		name: "Doraemon"
	}
}))
store.dispatch(storeAction.setListManager(USERLIST, {		// delete existing user
	type: "DELETE",
	payload: {
		id: 1,
		name: "Giant"
	}
}))
store.dispatch(storeAction.setListManager(USERLIST, {		// clear all user
	type: "CLEAR",
	payload: null
}))

// Stop listening to state updates
unsubscribe()