import { combineReducers } from 'redux'
import { INITIAL_STATE } from '../constants'
import axios from 'axios'

export class mainReducer {
  public todoUser(state: { user: Array<{ email: string, password: string }> } = INITIAL_STATE, action: {type: string, payload: null | { email: string, password: string }}) {
    switch (action.type) {
      case "USER.LOGIN": {
        if (action.payload) {
          axios.post("http://192.168.1.100:8080/login",
            {
              email: action.payload.email,
              password: action.payload.password
            }
          ).then(response => {
            console.log("Reducer2")
            if (response.data) {
              action.payload.token = response.data.token
              console.log("Reducer3")
              console.log(response.data)
              console.log(action.payload.token)
              console.log("Reducer3")
              return {
                ...state,
                user: [action.payload]
              }
            }
          })
          .catch(function (error) {
            console.log(error)
          })
        } else {
          return state
        }
      }
      case "USERLIST.DELETE": {
        if (action.payload) {
          const payloadUserId = action.payload.id
          return {
            ...state,
            user: state.user.filter(user => user.id !== payloadUserId)    // user match payload will be removed
          }
        } else {    // nothing in payload, so return previous state
          return state
        }
      }
      default:
        return state
    }
  }

  readonly todoApp = combineReducers({
    manageUser: this.todoUser
  })
}