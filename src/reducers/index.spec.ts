import { mainReducer } from './index'
import {
  INITIAL_STATE,
  USERLIST,
  SET_VISIBILITY_FILTER,
  VisibilityFilters
} from '../constants'

let testReducer = new mainReducer();  // declare Reducer Class

describe('visibility filter', () => {   // visibility filter test
  it('visibility filter SHOW_ALL, SHOW_COMPLETED, SHOW_ACTIVE', () => {
    const resultState = { SHOW_ALL: 'SHOW_ALL',
      SHOW_COMPLETED: 'SHOW_COMPLETED',
      SHOW_ACTIVE: 'SHOW_ACTIVE' }
    expect(testReducer.visibilityFilter(VisibilityFilters, SET_VISIBILITY_FILTER)).toEqual(resultState)
  })
})

describe('todos', () => {   // insert new user test
  it('todos insert new user', () => {
    const action = {
      type: "USERLIST.UPSERT",
      payload: {
        id: 3,
        name: "Doramomon"
      }
    }
    const resultState = { user:
       [ { id: 1, name: 'Giant' },
         { id: 2, name: 'Suneo' },
         { id: 3, name: 'Doramomon' } ] }
    expect(testReducer.todos(INITIAL_STATE, action)).toEqual(resultState)
  })
})

describe('todos', () => {   // update current user test
  it('todos update current user', () => {
    const action = {
      type: "USERLIST.UPSERT",
      payload: {
        id: 3,
        name: "Doraemon"
      }
    }
    const resultState = { user:
       [ { id: 1, name: 'Giant' },
         { id: 2, name: 'Suneo' },
         { id: 3, name: 'Doraemon' } ] }
    expect(testReducer.todos(INITIAL_STATE, action)).toEqual(resultState)
  })
})

describe('todos', () => {   // delete user with id 1
  it('todos delete current user', () => {
    const action = {
      type: "USERLIST.DELETE",
      payload: {
        id: 1,
        name: "Giant"
      }
    }
    const resultState = { user: [ { id: 2, name: 'Suneo' } ] }
    expect(testReducer.todos(INITIAL_STATE, action)).toEqual(resultState)
  })
})

describe('todos', () => {   // clear all user in list
  it('todos clear all user', () => {
    const action = {
      type: "USERLIST.CLEAR",
      payload: null
    }
    const resultState = { user: [ ] }
    expect(testReducer.todos(INITIAL_STATE, action)).toEqual(resultState)
  })
})