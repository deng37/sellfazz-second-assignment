import { createStackNavigator } from 'react-navigation';
import Home from '../screens/HomeScreen';
import Login from '../screens/LoginScreen';
import Register from '../screens/RegisterScreen';
import Main from '../screens/MainScreen';

const AppNavigator = createStackNavigator({
  Home: { screen: Home },
  Login: { screen: Login},
  Register: { screen: Register},
  Main: { screen: Main},
});

export default AppNavigator;
